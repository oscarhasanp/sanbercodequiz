import React, { useState, createContext } from 'react'
import { FlatList,View,Text } from 'react-native';

export const RootContext = createContext();

const ConsumFlatList=()=>{
    return(
        <RootContext.Consumer>
            {({data})=>{
                   console.log(data) 
                  return(
                      <FlatList
                      data={data}
                      renderItem={({item})=>{
                          return(<View>
                              <Text>{item.name}</Text>
                          <Text>{item.position}</Text>
                          </View>)
                      }}
                      keyExtractor={(item,index)=>index.toString()}
                      >

                      </FlatList>
                  )
            }}
        </RootContext.Consumer>
    )
}
const ContextAPI = () => {

    const [ name, setName ] = useState([
        {
            name: 'Zakky Muhammad Fajar',
            position: 'Trainer 1 React Native Lanjutan'
        },
        {
            name: 'Mukhlis Hanafi',
            position: 'Trainer 2 React Native Lanjutan'
        }
    ])

    return (
        <RootContext.Provider value={{
            data:name
        }}>
           <ConsumFlatList></ConsumFlatList> 
        </RootContext.Provider>
    )
}


export default ContextAPI;
