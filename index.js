/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './soal2';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
